package com.tkacz;

import java.util.ArrayList;
import java.util.List;

public class SequenceAdder {


    public long summingNElements1(int n) {
        long sum = 0;
        for (int i = 0; i <= n; i++) {
            sum = sum + i;
        }
        return sum;
    }

    public long summingNElements2(int number) {
        long sum = 0;
        for (int i = 0; i <= number; i++) {

            sum = sum + i;
        }
        return sum;
    }

    public long summingNElements3(int n) {
        List<Integer> x = new ArrayList<>(n);
        for (int i = 0; i <= n; i++) {
            x.add(i);
        }
        return x.stream().mapToLong(w -> w.longValue()).sum();
    }

    public long summingNElements4(int n) {
        return (1L+n)*n/2L;
    }
}







package com.tkacz;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SequenceAdderTest {
    @Test
    void testSummingNElements1(){
        SequenceAdder sequenceAdder=new SequenceAdder();
        long startTime = System.currentTimeMillis();
        final long result = sequenceAdder.summingNElements1(1000_000_000);
        long endTime = System.currentTimeMillis();

        long duration = (endTime - startTime);
        assertEquals(500000000500000000l,result);
        if(duration>1000){
            fail("too long - "+duration);
        }
    }
    @Test
    void testSummingNElements2(){
        SequenceAdder sequenceAdder=new SequenceAdder();
        long startTime = System.currentTimeMillis();
        final long result = sequenceAdder.summingNElements2(1000_000_000);
        long endTime = System.currentTimeMillis();

        long duration = (endTime - startTime);
        assertEquals(500000000500000000l,result);
        if(duration>1000){
            fail("too long - "+duration);
        }
    }
    @Test
    void testSummingNElements3(){
        SequenceAdder sequenceAdder=new SequenceAdder();
        long startTime = System.currentTimeMillis();
        final long result = sequenceAdder.summingNElements3(10_000_000);
        long endTime = System.currentTimeMillis();

        long duration = (endTime - startTime);
        assertEquals(50000005000000l,result);
        if(duration>1000){
            fail("too long - "+duration);
        }
    }
    @Test
    void testSummingNElements4(){
        SequenceAdder sequenceAdder=new SequenceAdder();
        long startTime = System.currentTimeMillis();
        final long result = sequenceAdder.summingNElements4(1_000_000_000);
        long endTime = System.currentTimeMillis();

        long duration = (endTime - startTime);
        assertEquals(500000000500000000l,result);
        if(duration>100){
            fail("too long - "+duration);
        }
    }
}